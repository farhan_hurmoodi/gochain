package utilities

import (
	"math/rand"
	"os/exec"
	"reflect"
	"time"
)

func Take(rows interface{}, limit int) []interface{} {
	data := make([]interface{}, 0)

	switch reflect.TypeOf(rows).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(rows)
		for i := 0; i < MinOf(limit, s.Len()); i++ {
			data = append(data, s.Index(i).Interface())
		}
	}

	return data
}

func IntRange(min, max int) []int {
	ranger := make([]int, max-min+1)

	for i := range ranger {
		ranger = append(ranger, min+i)
	}

	return ranger
}

func RandomInt(ranger []int) int {

	return ranger[rand.Intn(len(ranger)-1)]
}

func MinOf(vars ...int) int {
	min := vars[0]

	for _, i := range vars {
		if min > i {
			min = i
		}
	}

	return min
}

func RunCMD(cmdLine string) ([]byte, error) {
	return exec.Command("bash", "-c", cmdLine).Output()
}

func UnixTimestampToDateTime(unix int) string {
	unixTimeUTC := time.Unix(int64(unix), 0)

	return unixTimeUTC.Format(time.RFC3339)
}
