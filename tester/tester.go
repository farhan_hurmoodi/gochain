package tester

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"math"
	"os"
	"strconv"
)

func Tester() {
	hasher()
	os.Exit(1)
}

func hasher() {
	res := int(math.Pow(float64(3), 2) - math.Pow(float64(2), 2))
	hasher := sha256.New()
	hasher.Write([]byte(strconv.Itoa((res))))
	sha1_hash := hex.EncodeToString(hasher.Sum(nil))
	fmt.Println(sha1_hash)
}
