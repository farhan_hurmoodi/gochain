package node

import (
	"context"
	"flag"
	"fmt"
	"gochain/api"
	"gochain/blockchain"
	"gochain/utilities"
	"net"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/google/uuid"
)

// tcpKeepAliveListener sets TCP keep-alive timeouts on accepted connections.
// It's used by ListenAndServe and ListenAndServeTLS so dead TCP connections
// (e.g. closing laptop mid-download) eventually go away. This is code from
// net/http/server.go.
type tcpKeepAliveListener struct {
	*net.TCPListener
}

// Accept accepts a TCP connection while setting keep-alive timeouts.
func (ln tcpKeepAliveListener) Accept() (c net.Conn, err error) {
	tc, err := ln.AcceptTCP()
	if err != nil {
		return
	}
	tc.SetKeepAlive(true)
	tc.SetKeepAlivePeriod(3 * time.Minute)
	return tc, nil
}

func Run() {
	// Read flags from CLI
	env := flag.String("env", "dev", "Selected environment: allowed -dev, -prd")
	port := flag.Int("port", 3001, "Port of your node")
	uuid := flag.String("uuid", uuid.New().String(), "Port of your node")
	flag.Parse()

	nodeInfo := blockchain.NodeInfo{
		Port:    *port,
		Address: ":" + strconv.Itoa(*port),
		UUID:    *uuid,
	}
	node := blockchain.Node{
		Version:    blockchain.VERSION,
		PID:        os.Getpid(),
		ENV:        *env,
		ApiPrefix:  "/api",
		Info:       nodeInfo,
		Blockchain: *blockchain.NewBlockchain(),
	}

	blockchain.SetSingletonNode(node)

	fmt.Println("Running node "+nodeInfo.UUID+" on ", nodeInfo.Address)

	KillPortPorcessor(&node)
	Serve(&node)
}

func KillPortPorcessor(node *blockchain.Node) {
	cmdLine := "lsof -t -i tcp:" + strconv.Itoa(node.Info.Port) + " | xargs kill"
	_, err := utilities.RunCMD(cmdLine)
	if err != nil {
		fmt.Printf("%s\n", err)
		return
	}
	fmt.Println("Command Successfully Executed")
}

// https://github.com/rcrowley/goagain/blob/master/example/single/main.go
// Serve starts an HTTP server using the DefaultServeMux. This function does
// not return until the server has been stopped and all requests have finished.
//
// The server is controlled using Unix signals:
//
//   kill -s SIGINT <pid> (or Ctrl-C) stops the server, waiting for any active
//   requests to finish.
//
//   kill -s SIGUSR1 <pid> restarts the server with no downtime, using its (new)
//   binary. Current requests will finish before the old server exits and while
//   the new server already processes new requests.
//
//   kill -s SIGTERM <pid> stops the server without waiting for active requests
//   to finish.
func Serve(node *blockchain.Node) error {
	// The general idea for this is described here:
	// https://grisha.org/blog/2014/06/03/graceful-restart-in-golang/

	// We need to shut down gracefully when the user hits Ctrl-C.
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGUSR1, syscall.SIGTERM)

	nodeInfo := node.Info
	// Create a server.
	server := &http.Server{
		Addr:         nodeInfo.Address,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		Handler:      api.NewRouter(node),
	}

	// Start the server.
	var (
		listener *net.TCPListener
		wg       sync.WaitGroup
	)

	// http.Handle("/", api.NewRouter())
	wg.Add(1)
	go func() {
		defer wg.Done()

		// If this is a forked child process, we'll use its connection.
		isFork := os.Getenv("FORKED_SERVER") != ""

		var (
			ln  net.Listener
			err error
		)
		if isFork {
			// It's a fork. Get the file that was handed over.
			fmt.Printf("%d Getting existing listener for %s\n", node.PID, nodeInfo.Address)
			file := os.NewFile(3, "")
			ln, err = net.FileListener(file)
			if err != nil {
				fmt.Printf("%d Cannot use existing listener: %s\n", node.PID, err)
				sig <- syscall.SIGTERM
				return
			}

			// Tell the parent to stop the server now.
			parent := syscall.Getppid()
			fmt.Printf("%d Telling parent process (%d) to stop server\n", node.PID, parent)
			syscall.Kill(parent, syscall.SIGTERM)

			// Give the parent some time.
			time.Sleep(100 * time.Millisecond)
		} else {
			// It's a new server.
			fmt.Printf("%d Starting web server on %s\n", node.PID, nodeInfo.Address)
			ln, err = net.Listen("tcp", nodeInfo.Address)
			if err != nil {
				fmt.Printf("%d Cannot listen to %s: %s\n", node.PID, nodeInfo.Address, err)
				sig <- syscall.SIGTERM
				return
			}
		}

		// We can start the server now.
		fmt.Println(node.PID, "Serving requests...")
		listener = ln.(*net.TCPListener)
		if err = server.Serve(tcpKeepAliveListener{listener}); err != nil {
			fmt.Printf("%d Web server was shut down: %s\n", node.PID, err)
			// fmt.Printf("%d Reinitialize Web server...\n", node.PID)
			// Serve()
		}
		fmt.Println(node.PID, "Web server has finished")
	}()

	// Wait for the interrupt signal.
	s := <-sig
	switch s {
	case syscall.SIGTERM:
		// Go for the program exit. Don't wait for the server to finish.
		fmt.Println(node.PID, "Received SIGTERM, exiting without waiting for the web server to shut down")
		// return nil
	case syscall.SIGINT:
		// Stop the server gracefully.
		fmt.Println(node.PID, "Received SIGINT")
	case syscall.SIGUSR1:
		// Spawn a child process.
		fmt.Println(node.PID, "Received SIGUSR1")
		var args []string
		if len(os.Args) > 1 {
			args = os.Args[1:]
		}
		file, err := listener.File()
		if err != nil {
			fmt.Printf("%d Listener did not return file, not forking: %s\n", node.PID, err)
		} else {
			cmd := exec.Command(os.Args[0], args...)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			cmd.ExtraFiles = []*os.File{file}
			cmd.Env = append(os.Environ(), "FORKED_SERVER=1")
			if err := cmd.Start(); err != nil {
				fmt.Printf("%d Fork did not succeed: %s\n", node.PID, err)
			}
			fmt.Printf("%d Started child process %d, waiting for its ready signal\n", node.PID, cmd.Process.Pid)

			// We have a child process. A SIGTERM means the child process is ready to
			// start its server.
			<-sig
		}
	}

	// Force the server to shut down.
	fmt.Println(node.PID, "Shutting down web server and waiting for requests to finish...")
	defer fmt.Println(node.PID, "Requests have finished")
	if err := server.Shutdown(context.Background()); err != nil {
		return fmt.Errorf("Shutdown failed: %s", err)
	}
	wg.Wait()

	return nil
}
