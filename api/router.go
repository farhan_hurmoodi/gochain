package api

import (
	"gochain/blockchain"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	jsoniter "github.com/json-iterator/go"
	"github.com/rs/cors"
)

// ------ Vars ------ //

var json = jsoniter.ConfigCompatibleWithStandardLibrary

// ------ Public ------ //

func NewRouter(node *blockchain.Node) http.Handler {
	rtr := mux.NewRouter().StrictSlash(true)
	rtr.Use(middleware)

	// Blockchain
	rtr.HandleFunc(node.ApiPrefix+"/mine_block", MineBlock).Methods("GET")
	rtr.HandleFunc(node.ApiPrefix+"/get_chain", GetChain).Methods("GET")
	rtr.HandleFunc(node.ApiPrefix+"/is_chain_valid", IsChainValid).Methods("GET")
	rtr.HandleFunc(node.ApiPrefix+"/add_transaction", AddTransaction).Methods("POST")
	rtr.HandleFunc(node.ApiPrefix+"/connect_node", ConnectNode).Methods("POST")
	rtr.HandleFunc(node.ApiPrefix+"/replace_chain", ReplaceChain).Methods("GET")

	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		// AllowCredentials: false,
		// OptionsPassthrough: true,
		// AllowedMethods: []string{"*"},
		AllowedHeaders: []string{"Authorization", "Content-Encoding", "Content-Type"},
		// Enable Debugging for testing, consider disabling in production
		// Debug: true,
	})

	return c.Handler(rtr)
}

func RespondWithJSON(w http.ResponseWriter, r *http.Request, code int, payload interface{}) {
	jsonString, err := json.Marshal(&payload)

	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(jsonString)
}

func RespondWithError(w http.ResponseWriter, r *http.Request, code int, message string) {
	RespondWithJSON(w, r, code, map[string]string{"error": message})
}

// ------ Private ------ //

func logger(text string) {
	f, _ := os.OpenFile("logs.txt", os.O_APPEND|os.O_WRONLY, 0644)
	f.WriteString(text + "\n")
	f.Close()
}

func middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r)
	})
}
