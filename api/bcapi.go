package api

import (
	"gochain/blockchain"
	"gochain/utilities"
	"io/ioutil"
	"log"
	"strconv"
	"time"

	"net/http"
	"net/url"
)

// ------ Structs ------ //

type GenericError struct {
	Error string `json:"error"`
}

type GenericMessage struct {
	Message string `json:"message"`
}

type MinedBlock struct {
	Index        int                      `json:"index"`
	Message      string                   `json:"message"`
	PreviousHash string                   `json:"previous_hash"`
	Proof        int                      `json:"proof"`
	Timestamp    string                   `json:"timestamp"`
	Transactions []blockchain.Transaction `json:"transactions"`
}

type FullChainResponse struct {
	Chain  []blockchain.Block `json:"chain"`
	Length int                `json:"length"`
}

type IsChainValidResponse struct {
	Message string `json:"message"`
	Valid   bool   `json:"valid"`
}

type ConnectingNode struct {
	Address string `json:"address"`
	UUID    string `json:"uuid"`
	Version int    `json:"version"`
}

type ConnectingNodes struct {
	Nodes []ConnectingNode `json:"nodes"`
}

// ------ Public ------ //

func MineBlock(w http.ResponseWriter, r *http.Request) {
	uuid := r.URL.Query()["uuid"]
	if len(uuid) == 0 {
		RespondWithJSON(w, r, http.StatusBadRequest, GenericError{Error: "Missing UUID of your node!"})
		return
	}
	_, err := url.Parse(r.Host)
	if err != nil {
		RespondWithJSON(w, r, http.StatusNotAcceptable, GenericError{Error: "Couldn't identify your host!"})
		return
	}

	_node := blockchain.SingletonNode
	_blockchain := &_node.Blockchain
	prevBlock := _blockchain.GetPreviousBlock()
	prevProof := prevBlock.Proof
	proof := _blockchain.ProofOfWork(prevProof)
	prevHash := prevBlock.Hash()
	_blockchain.AddTransaction(_node.Info.UUID, uuid[0], blockchain.DEFAULT_MINING_REWARD)
	block := _blockchain.CreateBlock(proof, prevHash)
	resp := MinedBlock{
		Index:        block.Index,
		Message:      "Congrats! You just mined a block!",
		PreviousHash: block.PreviousHash,
		Proof:        block.Proof,
		Timestamp:    utilities.UnixTimestampToDateTime(block.Timestamp),
		Transactions: block.Transactions,
	}

	block.Transactions = nil

	RespondWithJSON(w, r, http.StatusOK, resp)

	go func() {
		blockMined()
	}()
}

func GetChain(w http.ResponseWriter, r *http.Request) {
	_blockchain := &blockchain.SingletonNode.Blockchain
	resp := FullChainResponse{
		Chain:  _blockchain.Chain,
		Length: len(_blockchain.Chain),
	}

	RespondWithJSON(w, r, http.StatusOK, resp)
}

func IsChainValid(w http.ResponseWriter, r *http.Request) {
	_blockchain := &blockchain.SingletonNode.Blockchain
	isValid := _blockchain.IsChainValid(_blockchain.Chain)
	message := "The chain is valid. :)"
	if !isValid {
		message = "The chain is NOT valid!! :("
	}
	resp := IsChainValidResponse{
		Message: message,
		Valid:   isValid,
	}

	RespondWithJSON(w, r, http.StatusOK, resp)
}

func AddTransaction(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		RespondWithJSON(w, r, http.StatusBadRequest, GenericError{Error: "Something went wrong when reading your transaction."})
		return
	}
	var transaction blockchain.Transaction
	err = json.Unmarshal(body, &transaction)
	if err != nil {
		RespondWithJSON(w, r, http.StatusBadRequest, GenericError{Error: "Something went wrong when reading your transaction."})
		return
	}
	missingArg := ""

	if transaction.Sender == "" {
		missingArg = "sender"
	} else if transaction.Rceiver == "" {
		missingArg = "receiver"
	}

	if missingArg != "" {
		RespondWithJSON(w, r, http.StatusBadRequest, GenericError{Error: "Missing " + missingArg + " in your POST body."})
		return
	}

	if transaction.Amount <= 0 {
		RespondWithJSON(w, r, http.StatusBadRequest, GenericError{Error: "Cannot add transaction for amount less than or equal to zero."})
		return
	}

	_blockchain := &blockchain.SingletonNode.Blockchain
	nextBlock := _blockchain.AddTransaction(transaction.Sender, transaction.Rceiver, transaction.Amount)

	resp := GenericMessage{
		Message: "Your transaction has been added and will show up in block #" + strconv.Itoa(nextBlock),
	}

	RespondWithJSON(w, r, http.StatusOK, resp)
}

func ConnectNode_DEPRECATED(w http.ResponseWriter, r *http.Request) {
	uuid := r.URL.Query()["uuid"]
	if len(uuid) == 0 {
		RespondWithJSON(w, r, http.StatusBadRequest, GenericError{Error: "Please provide a UUID for your node!"})
		return
	}
	version := r.URL.Query()["version"]
	if len(version) == 0 {
		RespondWithJSON(w, r, http.StatusBadRequest, GenericError{Error: "Please provide your blockchain version!"})
		return
	}

	versionInt, err := strconv.Atoi(version[0])
	if err != nil || versionInt <= 0 {
		RespondWithJSON(w, r, http.StatusBadRequest, GenericError{Error: "Please make sure you are providing the correct blockchain version."})
		return
	}

	_blockchain := &blockchain.SingletonNode.Blockchain
	_blockchain.AddNode(r.Host, uuid[0], versionInt)

	resp := GenericMessage{
		Message: "Your node has been added to blockchain!",
	}

	RespondWithJSON(w, r, http.StatusOK, resp)
}

func ConnectNode(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		RespondWithJSON(w, r, http.StatusBadRequest, GenericError{Error: "Something went wrong when reading your node list."})
		return
	}
	var nodes ConnectingNodes
	err = json.Unmarshal(body, &nodes)
	if err != nil {
		RespondWithJSON(w, r, http.StatusBadRequest, GenericError{Error: "Something went wrong when reading your node list."})
		return
	}

	_blockchain := &blockchain.SingletonNode.Blockchain
	for _, node := range nodes.Nodes {
		_blockchain.AddNode(node.Address, node.UUID, node.Version)
	}

	resp := GenericMessage{
		Message: "You are now connected to the blockchain!",
	}

	RespondWithJSON(w, r, http.StatusOK, resp)
}

func ReplaceChain(w http.ResponseWriter, r *http.Request) {
	log.Println("shouldReplaceNode?", r.Host)
	resp := GenericMessage{
		Message: "Your chain is up to date.",
	}
	if shouldReplaceNode() {
		resp.Message = "Your chain has been updated!"
	}

	RespondWithJSON(w, r, http.StatusOK, resp)
}

// ------ Private ------ //

func shouldReplaceNode() bool {
	_blockchain := &blockchain.SingletonNode.Blockchain
	network := _blockchain.Nodes
	maxLength := len(_blockchain.Chain)
	longestChain := _blockchain.Chain
	netClient := &http.Client{
		Timeout: time.Second * 10,
	}

	for _, node := range network {
		response, err := netClient.Get("http://" + node.Info.Address + node.ApiPrefix + "/get_chain")
		if err != nil {
			continue
		}

		defer response.Body.Close()
		if response.StatusCode == http.StatusOK {
			var chainResponse FullChainResponse
			body, err := ioutil.ReadAll(response.Body)
			err = json.Unmarshal(body, &chainResponse)
			if err != nil {
				continue
			}

			if chainResponse.Length > maxLength && _blockchain.IsChainValid(chainResponse.Chain) {
				maxLength = chainResponse.Length
				longestChain = chainResponse.Chain
			}
		}
	}

	if maxLength > len(_blockchain.Chain) {
		_blockchain.Chain = longestChain
		return true
	}

	return false
}

func blockMined() {
	netClient := &http.Client{
		Timeout: time.Second * 10,
	}

	for _, node := range blockchain.SingletonNode.Blockchain.Nodes {
		netClient.Get("http://" + node.Info.Address + node.ApiPrefix + "/replace_chain")
	}
}
