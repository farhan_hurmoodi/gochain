package blockchain

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"math"
	"net/url"
	"strconv"
	"time"
)

// ------ Structs ------ //

type Transaction struct {
	Sender  string  `json:"sender"`
	Rceiver string  `json:"receiver"`
	Amount  float64 `json:"amount"`
}

type Block struct {
	Index        int           `json:"index"`
	Timestamp    int           `json:"timestamp"`
	Proof        int           `json:"proof"`
	PreviousHash string        `json:"previous_hash"`
	Transactions []Transaction `json:"transactions"`
}

type Blockchain struct {
	Chain        []Block       `json:"chain"`
	Nodes        []Node        `json:"nodes"`
	Transactions []Transaction `json:"transactions"`
}

// ------ Vars ------ //

var LEADING_ZEROS = "0000"
var DEFAULT_MINING_REWARD = float64(1)
var VERSION = 1

// ------ Public ------ //

func NewBlockchain() *Blockchain {
	println("NewBlockchain...")
	bc := Blockchain{}
	// create the legacy block
	bc.CreateBlock(0, "0000000000000000000000000000000000000000000000000000000000000000")
	return &bc
}

func (bc *Blockchain) CreateBlock(proof int, prevousHash string) Block {
	block := Block{
		Index:        len(bc.Chain) + 1,
		Timestamp:    int(time.Now().Unix()),
		Proof:        proof,
		PreviousHash: prevousHash,
		Transactions: bc.Transactions,
	}

	bc.Transactions = []Transaction{}
	bc.Chain = append(bc.Chain, block)

	return block
}

func (bc *Blockchain) AddNode(address, uuid string, version int) (bool, error) {
	uri, err := url.Parse(address)
	if err != nil {
		return false, err
	}

	port, _ := strconv.Atoi(uri.Port())
	nodeInfo := NodeInfo{
		Address: uri.Host,
		Port:    port,
		UUID:    uuid,
	}
	node := Node{
		Version:   version,
		Info:      nodeInfo,
		ApiPrefix: "/api",
	}
	bc.Nodes = append(bc.Nodes, node)

	return true, nil
}

func (bc *Blockchain) AddTransaction(sender, receiver string, amount float64) int {
	transaction := Transaction{
		Sender:  sender,
		Rceiver: receiver,
		Amount:  amount,
	}
	bc.Transactions = append(bc.Transactions, transaction)

	return bc.GetPreviousBlock().Index + 1
}

func (bc *Blockchain) GetPreviousBlock() Block {
	return bc.Chain[len(bc.Chain)-1]
}

func (bc *Blockchain) ProofOfWork(previousProof int) int {
	newProof := 1
	checkProof := false

	for !checkProof {
		hash := hashOfProoves(newProof, previousProof)
		if hash[:len(LEADING_ZEROS)] == LEADING_ZEROS {
			checkProof = true
		} else {
			newProof += 1
		}
	}

	return newProof
}

func (bc *Blockchain) IsChainValid(chain []Block) bool {
	lengthOfChain := len(chain)

	for blockIndex := 1; blockIndex < lengthOfChain; blockIndex++ {
		if chain[blockIndex-1].Hash() != chain[blockIndex].PreviousHash {
			return false
		}
		hash := hashOfProoves(chain[blockIndex].Proof, chain[blockIndex-1].Proof)
		if hash[:len(LEADING_ZEROS)] != LEADING_ZEROS {
			return false
		}
	}

	return true
}

func (block *Block) Hash() string {
	text, _ := json.Marshal(block)
	hasher := sha256.New()
	hasher.Write([]byte(text))
	sha1_hash := hex.EncodeToString(hasher.Sum(nil))

	return sha1_hash
}

// ------ Private ------ //

// Very simple algorithm to generate hash of prooves based on two integers
func hashOfProoves(newProof, previousProof int) string {
	num := int(math.Pow(float64(newProof), 2) - math.Pow(float64(previousProof), 2))
	hasher := sha256.New()
	hasher.Write([]byte(strconv.Itoa((num))))
	sha1_hash := hex.EncodeToString(hasher.Sum(nil))
	// println(newProof)
	// println(sha1_hash)

	return sha1_hash
}
