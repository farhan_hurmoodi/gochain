package blockchain

// ------ Structs ------ //

type NodeInfo struct {
	Address string `json:"address"`
	Port    int    `json:"port"`
	UUID    string `json:"uuid"`
}

type Node struct {
	Version    int        `json:"version"`
	PID        int        `json:"pid"`
	ENV        string     `json:"env"`
	ApiPrefix  string     `json:"api_prefix"`
	Info       NodeInfo   `json:"info"`
	Blockchain Blockchain `json:"blockchain"`
}

// ------ Vars ------ //

var SingletonNode *Node

// ------ Public ------ //

func SetSingletonNode(node Node) {
	SingletonNode = &node
}

func GetSingletonNode() *Node {
	return SingletonNode
}
